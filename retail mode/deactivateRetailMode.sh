#!/bin/bash
adb shell am start -S "com.android.settings/com.android.settings.DeviceAdminSettings"
echo "--------------------"
read -p "Deactivate 'Retail Demo' then press any key to continue... " -n1 -s

echo -e "\n\nremoving retail mode"
adb shell pm uninstall -k --user 0 com.huawei.retaildemo

echo -e "\n\nremoving device specific retail mode apps"
adb shell pm uninstall -k --user 0 com.huawei.experience.sne

echo -e "\n\ntemporarily remove live demo notification"
adb shell pm clear com.android.systemui